# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class AccountMove(models.Model):
    _inherit = 'account.move'

    create_date_tmp = fields.Datetime('Creation Date')
    custom_write_date = fields.Datetime(string='Last Modified')
    order_reference = fields.Char(compute='_compute_order_reference', string='Order Reference')
    amount_total_signed = fields.Monetary(string='Total', store=True, readonly=True,
        compute='_compute_amount', currency_field='company_currency_id')
    invoice_partner_display_name = fields.Char(compute='_compute_invoice_partner_display_info', store=True, string='Customer')

    def _compute_order_reference(self):
        for obj in self:
            obj.order_reference = obj.name

    # Method overridden
    def _get_name_invoice_report(self):
        """ This method need to be inherit by the localizations if they want to print a custom invoice report instead of
        the default one. For example please review the l10n_ar module """
        self.ensure_one()
        return 'metroerp_customizations.report_invoice_inherit_document'

    # Custom Terms and Conditions
    @api.onchange('move_type')
    def _onchange_type(self):
        ''' Onchange made to filter the partners depending of the type. '''
        if self.is_sale_document(include_receipts=True):
            if self.env['ir.config_parameter'].sudo().get_param('use_sales_invoice_tc'):
                self.narration = self.company_id.sales_invoice_tc or self.env.company.sales_invoice_tc
        if self.is_purchase_document(include_receipts=True):
            if self.env['ir.config_parameter'].sudo().get_param('use_purchase_invoice_tc'):
                self.narration = self.company_id.purchase_invoice_tc or self.env.company.purchase_invoice_tc

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        res = super(AccountMove, self)._onchange_partner_id()
        if self.is_sale_document(include_receipts=True) and self.partner_id:
            self.narration = self.company_id.with_context(lang=self.partner_id.lang or self.env.lang).purchase_invoice_tc
        if self.is_purchase_document(include_receipts=True) and self.partner_id:
            self.narration = self.company_id.with_context(lang=self.partner_id.lang or self.env.lang).purchase_invoice_tc
        return res

    @api.model
    def create(self, vals):
        if vals.get('narration'):
            if vals.get('move_type') and vals.get('move_type') in ['out_invoice', 'out_refund'] and self.env['ir.config_parameter'].sudo().get_param('use_sales_invoice_tc'):
                vals['narration'] = self.env.company.sales_invoice_tc or ''
            elif vals.get('move_type') and vals.get('move_type') in ['in_invoice', 'in_refund'] and self.env['ir.config_parameter'].sudo().get_param('use_purchase_invoice_tc'):
                vals['narration'] = self.env.company.purchase_invoice_tc or ''
        res = super().create(vals)
        if 'create_date_tmp' in vals:
            self.env.cr.execute("UPDATE account_move set create_date = %s where id = %s", (vals.get('create_date_tmp'), res.id))
        else:
            res.create_date_tmp = res.create_date
        if 'custom_write_date' in vals:
            res.write({'write_date': vals['custom_write_date']})
        else:
            res.write({'custom_write_date': fields.Datetime.now()})
        return res

    def write(self, vals):
        if 'custom_write_date' not in vals:
            vals['custom_write_date'] = fields.Datetime.now()
        return super(AccountMove, self).write(vals)

    def generate_xlsx_customized_report(self):
        return {
           'type': 'ir.actions.act_url',
           'url': '/invoicing/excel_report/%s' % (str(self.ids)),
           'target': 'new',
        }

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    @api.onchange('product_id')
    def _onchange_product_id(self):
        """ Inherited Method """
        result = super(AccountMoveLine, self)._onchange_product_id()
        ctx = self._context
        # WHEN Created the Invoice/Bill/Creditnotes/refunds manually/directly.
        if ctx.get('default_move_type') in ['out_invoice','out_refund']:
            if not self.product_id.description_sale:
                self.name = " "
            elif self.product_id.description_sale:
                description_sale = self.product_id.description_sale.strip()
                if not description_sale:
                    description_sale = " "
                self.name = description_sale
            return result
        elif ctx.get('default_move_type') in ['in_invoice','in_refund']:
            if not self.product_id.description_purchase:
                self.name = " "
            elif self.product_id.description_purchase:
                description_purchase = self.product_id.description_purchase.strip()
                if not description_purchase:
                    description_purchase = " "
                self.name = description_purchase
            return result