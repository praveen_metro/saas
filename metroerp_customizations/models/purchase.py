# -*- coding: utf-8 -*-

from odoo import api, fields, models


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    @api.model
    def _purchase_default_note(self):
        return self.env['ir.config_parameter'].sudo().get_param('use_purchase_tc') and self.env.company.purchase_tc or ''

    notes = fields.Text('Terms and Conditions', default=_purchase_default_note)
    create_date_tmp = fields.Datetime('Creation Date')

    @api.model
    def create(self, vals):
        res = super(PurchaseOrder, self).create(vals)
        if 'create_date_tmp' in vals:
            self.env.cr.execute("UPDATE purchase_order set create_date = %s where id = %s", (vals.get('create_date_tmp'), res.id))
        else:
            res.create_date_tmp = res.create_date
        return res

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    @api.onchange('product_id')
    def onchange_product_id(self):
        """ Inherited Method """
        if not self.product_id:
            return
        result = super(PurchaseOrderLine, self).onchange_product_id()
        if not self.product_id.description_purchase:
            self.name = " "
        elif self.product_id.description_purchase:
            description_purchase = self.product_id.description_purchase.strip()
            if not description_purchase:
                description_purchase = " "
            self.name = description_purchase
        return result

    def _get_product_purchase_description(self, product_lang):
        """ Overidden Method: removed the appending of name to the description. """
        self.ensure_one()
        name = product_lang.display_name
        if product_lang.description_purchase:
            name = product_lang.description_purchase
        return name