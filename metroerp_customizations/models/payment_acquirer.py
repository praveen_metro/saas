# -*- coding: utf-8 -*-
from odoo import models, fields, api

class PaymentAcquirer(models.Model):
    _inherit = 'payment.acquirer'

    module_to_buy = fields.Boolean(string='ERP Enterprise Module', related='module_id.to_buy', readonly=True, store=False)