# -*- coding: utf-8 -*-

from . import res_users
from . import res_groups
from . import ir_mail_server
from . import res_partner
from . import ir_ui_menu
from . import res_company
from . import res_config_settings
from . import mail_channel
from . import digest
from . import ir_model_access
from . import account_move
from . import ir_module
from . import payment_acquirer
from . import digest_tip
from . import mail_message
from . import ir_actions_act_window
from . import sale
from . import purchase
from . import product
from . import base_models
from . import sr_purchase_price_history
from . import sr_sale_price_history
from . import stock_picking
from . import chart_template
from . import mail_followers
from . import account_journal