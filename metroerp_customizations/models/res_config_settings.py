# -*- coding: utf-8 -*-
import base64, os
from odoo import fields, models, api, tools
import logging
from lxml import etree

_logger = logging.getLogger(__name__)


class IrDefault(models.Model):
    _inherit = 'ir.default'

    @api.model
    def set_wk_favicon(self, model, field):
        script_dir = os.path.dirname(__file__)
        rel_path = "../static/src/img/favicon.png"
        abs_file_path = os.path.join(script_dir, rel_path)
        with open(abs_file_path, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())
            self.set('res.config.settings', 'wk_favicon', encoded_string.decode("utf-8"))


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    auth_signup_uninvited = fields.Selection([
        ('b2b', 'On invitation'),
        ('b2c', 'Free sign up'),
    ], string='Customer Account', default='b2b', config_parameter='auth_signup.invitation_scope')
    module_partner_autocomplete = fields.Boolean("Partner Autocomplete", default=False)

    wk_favicon = fields.Binary(string="Favicon Image")
    title_brand = fields.Char(string="Title Brand")
    odoo_text_replacement = fields.Char(string='Replace Text "Odoo" With?')
    favicon_url = fields.Char(string="Url")
    attach_id = fields.Integer(string="Favicon Attach ID")
    # Base fields overidden to add the groups
    recaptcha_public_key = fields.Char("Site Key", config_parameter='recaptcha_public_key', groups='base.group_system,metroerp_customizations.sub_admin_group')
    recaptcha_private_key = fields.Char("Secret Key", config_parameter='recaptcha_private_key', groups='base.group_system,metroerp_customizations.sub_admin_group')
    recaptcha_min_score = fields.Float("Minimum score", config_parameter='recaptcha_min_score', groups='base.group_system,metroerp_customizations.sub_admin_group', default="0.5", help="Should be between 0.0 and 1.0")

    sales_tc = fields.Text(related='company_id.sales_tc', readonly=False)
    use_sales_tc = fields.Boolean(string='Sales Quotation Terms & Conditions', config_parameter='use_sales_tc')
    
    sales_invoice_tc = fields.Text(related='company_id.sales_invoice_tc', readonly=False)
    use_sales_invoice_tc = fields.Boolean(string='Sales Invoice Terms & Conditions', config_parameter='use_sales_invoice_tc')

    purchase_tc = fields.Text(related='company_id.purchase_tc', readonly=False)
    use_purchase_tc = fields.Boolean(string='Purchase Quotation Terms & Conditions', config_parameter='use_purchase_tc')
    
    purchase_invoice_tc = fields.Text(related='company_id.purchase_invoice_tc', readonly=False)
    use_purchase_invoice_tc = fields.Boolean(string='Purchase Invoice Terms & Conditions', config_parameter='use_purchase_invoice_tc')
    sale_order_line_record_limit = fields.Integer(string="Record Limit", default=10,
                                                  config_parameter='sale_order_line_record_limit')
    sale_order_status = fields.Selection([('sale', 'Confirm order'), ('done', 'Done (Locked)'), ('both', 'Both')],
                                         string="Price History Based On", default="sale",
                                         config_parameter='sale_order_status')
    purchase_order_line_record_limit = fields.Integer(string="Record Limit", default=10,
                                                      config_parameter='purchase_order_line_record_limit')
    purchase_order_status = fields.Selection(
        [('purchase', 'Purchase order'), ('done', 'Done (Locked)'), ('both', 'Both')], string="Price History Based On",
        default="purchase", config_parameter='purchase_order_status')

    active_user_count = fields.Integer('Number of Active Users', compute="_compute_active_user_count")
    company_count = fields.Integer('Number of Companies', compute="_compute_company_count")
    chart_of_accounts_installed = fields.Boolean(related="company_id.chart_of_accounts_installed", string="Chart of Accounts Installed")

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        if self.env.company == self.company_id and self.chart_template_id and self.chart_template_id != self.company_id.chart_template_id:
            self.chart_template_id._load(15.0, 15.0, self.env.company)
            self.company_id.chart_of_accounts_installed = True
    
            
    def install_chart_of_account(self):
        if self.env.company == self.company_id and self.chart_template_id:
            self.chart_template_id._load(15.0, 15.0, self.env.company)
            self.company_id.chart_of_accounts_installed = True


    @api.depends('company_id')
    def _compute_company_count(self):
        company_count = self.env['res.company'].search_count([])
        for record in self:
            record.company_count = company_count

    @api.depends('company_id')
    def _compute_active_user_count(self):
        active_user_count = self.env['res.users'].search_count([('share', '=', False)])
        for record in self:
            record.active_user_count = active_user_count

    @api.model
    def fields_view_get(
            self, view_id=None, view_type="form", toolbar=False, submenu=False
    ):
        ret_val = super().fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu
        )

        page_name = ret_val["name"]
        if not page_name == "res.config.settings.view.form":
            return ret_val

        doc = etree.XML(ret_val["arch"])

        query = "//div[div[field[@widget='upgrade_boolean']]]"
        for item in doc.xpath(query):
            item.attrib["class"] = "d-none"

        ret_val["arch"] = etree.tostring(doc)
        return ret_val

    def execute(self):
        """
        Called when settings are saved.

        This method will call `set_values` and will install/uninstall any modules defined by
        `module_` Boolean fields and then trigger a web client reload.

        .. warning::

            This method **SHOULD NOT** be overridden, in most cases what you want to override is
            `~set_values()` since `~execute()` does little more than simply call `~set_values()`.

            The part that installs/uninstalls modules **MUST ALWAYS** be at the end of the
            transaction, otherwise there's a big risk of registry <-> database desynchronisation.
        """
       
        self.ensure_one()
        if not self.env.is_admin():
            raise AccessError(_("Only administrators can change the settings"))

        self = self.with_context(active_test=False)
        classified = self._get_classified_fields()

        self.set_values()

        # module fields: install/uninstall the selected modules
        to_install = []
        to_uninstall_modules = self.env['ir.module.module']
        lm = len('module_')
        for name, module in classified['module']:
            if int(self[name]):
                to_install.append((name[lm:], module))
            else:
                if module and module.state in ('installed', 'to upgrade'):
                    to_uninstall_modules += module

        if to_install or to_uninstall_modules:
            self.flush()

        if to_uninstall_modules:
            to_uninstall_modules.button_immediate_uninstall()

        installation_status = self._install_modules(to_install)
        
        """Custom Code start """
        """Here we are overridingg the for erp_admin groups for (recaptcha_public_key,recaptcha_private_key,recaptcha_min_score)
           when we make reCAPTCHA: Easy on Humans, Hard on Bots is True  and Save then google_recaptcha module will install,
           when we make reCAPTCHA: Easy on Humans, Hard on Bots is False and Save then google_recaptcha module will install
           when we install the google_recaptcha module autoatically metroerp_customizations will upgrade then erp_admin will see 
           (recaptcha_public_key,recaptcha_private_key,recaptcha_min_score) fields. 
           """
        for x,y in to_install:
            if str(x) == 'google_recaptcha':
                to_upgrade_modules = self.env['ir.module.module'].search([('name','=','metroerp_customizations')])
                to_upgrade_modules.button_immediate_upgrade()
        """Custom Code END"""

        if installation_status or to_uninstall_modules:
            # After the uninstall/install calls, the registry and environments
            # are no longer valid. So we reset the environment.
            self.env.reset()
            self = self.env()[self._name]

        # pylint: disable=next-method-called
        config = self.env['res.config'].next() or {}
        if config.get('type') not in ('ir.actions.act_window_close',):
            return config

        # force client-side reload (update user menu and current view)
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }
    
    @api.model
    def _update_digest_settings(self):
        """While Upgrading Metrogroup customization module Updating the digest email name and State values"""
        digest = self.env['digest.digest'].sudo().search([])
        if digest :
            for record in digest:
                if "Odoo" in record.name:
                    new_name = (record.name).replace("Odoo", "ERP")
                    record.write({'name':new_name,'state':'deactivated'})
        return True

    @api.model
    def get_debranding_settings(self):
        IrDefault = self.env['ir.default'].sudo()
        wk_favicon = IrDefault.get('res.config.settings', "wk_favicon")
        title_brand = IrDefault.get('res.config.settings', "title_brand")
        odoo_text_replacement = IrDefault.get('res.config.settings', "odoo_text_replacement")
        favicon_url = IrDefault.get('res.config.settings', "favicon_url")
        attach_id = IrDefault.get('res.config.settings', "attach_id")
        return {
            'wk_favicon': wk_favicon,
            'attach_id': attach_id,
            'title_brand': title_brand,
            'odoo_text_replacement': odoo_text_replacement,
            'favicon_url': favicon_url,
        }

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        IrDefault = self.env['ir.default'].sudo()
        IrDefault.set('res.config.settings', "wk_favicon", self.wk_favicon.decode("utf-8"))
        IrDefault.set('res.config.settings', "title_brand", self.title_brand)
        IrDefault.set('res.config.settings', "odoo_text_replacement", self.odoo_text_replacement)
        if not self.attach_id:
            attach_id = self.env['ir.attachment'].sudo().search([('name', '=', 'Favicon')])
            if attach_id:
                attach_id.write({
                    'datas': self.wk_favicon.decode("utf-8"),
                })
            else:
                attach_id = self.env['ir.attachment'].sudo().create({
                    'name': 'Favicon',
                    'datas': self.wk_favicon.decode("utf-8"),
                    'public': True
                })
        else:
            attach_id.write({
                'datas': self.wk_favicon.decode("utf-8"),
            })
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')

        image_url = base_url + '/web/image/?model=ir.attachment&id=' + str(attach_id.id) + '&field=datas'
        IrDefault.set('res.config.settings', "favicon_url", image_url)
        IrDefault.set('res.config.settings', "sale_order_line_record_limit", self.sale_order_line_record_limit)
        IrDefault.set('res.config.settings', "sale_order_status", self.sale_order_status)
        IrDefault.set('res.config.settings', "purchase_order_line_record_limit", self.purchase_order_line_record_limit)
        IrDefault.set('res.config.settings', "purchase_order_status", self.purchase_order_status)

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrDefault = self.env['ir.default'].sudo()
        wk_favicon = IrDefault.get('res.config.settings', "wk_favicon")
        title_brand = IrDefault.get('res.config.settings', "title_brand")
        odoo_text_replacement = IrDefault.get('res.config.settings', "odoo_text_replacement")
        favicon_url = IrDefault.get('res.config.settings', 'favicon_url')
        attach_id = IrDefault.get('res.config.settings', 'attach_id')
        sale_order_line_record_limit = IrDefault.get('res.config.settings', 'sale_order_line_record_limit')
        sale_order_status = IrDefault.get('res.config.settings', 'sale_order_status')
        purchase_order_line_record_limit = IrDefault.get('res.config.settings', 'purchase_order_line_record_limit')
        purchase_order_status = IrDefault.get('res.config.settings', 'purchase_order_status')
        res.update(
            wk_favicon=wk_favicon,
            title_brand=title_brand,
            odoo_text_replacement=odoo_text_replacement,
            favicon_url=favicon_url,
            attach_id=attach_id,
            sale_order_line_record_limit=int(sale_order_line_record_limit) if sale_order_line_record_limit else 10,
            sale_order_status=sale_order_status,
            purchase_order_line_record_limit=int(purchase_order_line_record_limit) if purchase_order_line_record_limit else 10,
            purchase_order_status=purchase_order_status
        )
        if not self.chart_template_id:
            res.update(
                chart_template_id=self.env.ref('l10n_sg.sg_chart_template').id,
            )
        return res

    @api.depends('company_id')
    def _compute_active_user_count(self):
        """ Overidden for the sake of new group 'ERP Admin'. """
        if self.env.user.has_group('metroerp_customizations.sub_admin_group') and not self.env.user.has_group(
                'base.group_erp_manager'):
            active_user_count = self.env['res.users'].sudo().search_count(
                [('share', '=', False), ('is_admin_flag', '=', False)])
            for record in self:
                record.active_user_count = active_user_count
        else:
            active_user_count = self.env['res.users'].sudo().search_count([('share', '=', False)])
            for record in self:
                record.active_user_count = active_user_count
