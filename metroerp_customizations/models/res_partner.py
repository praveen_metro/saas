# -*- coding: utf-8 -*-
from odoo import models, fields, api
import re
from odoo.exceptions import ValidationError


class ResPartnerInherited(models.Model):
    _inherit = "res.partner"

    signup_token = fields.Char(copy=False, groups="base.group_erp_manager,metroerp_customizations.sub_admin_group")
    signup_type = fields.Char(string='Signup Token Type', copy=False,
                              groups="base.group_erp_manager,metroerp_customizations.sub_admin_group")
    signup_expiration = fields.Datetime(copy=False,
                                        groups="base.group_erp_manager,metroerp_customizations.sub_admin_group")
    company_id = fields.Many2one('res.company', 'Company', index=True) #default=lambda self: self.env.company.id

			
    # @api.constrains('mobile')
    # def validate_mobile_format(self):
    #     for partner in self:
    #         if partner.mobile:
    #             if not re.match(r'(^\+)?([\d]+)$', str(partner.mobile)):
    #                 raise ValidationError("The format of the 'Mobile' is improper.")
                
    @api.model
    def create(self, vals):
        """ To set the partner with the logged in User's Company while creating a User. """
        # print("\n Partner Create() >>>>>>")
        # print("vals ===",vals)
        ctx = self._context
        # print("ctxxxxxx ====",ctx)
        # if 'params' in ctx and ctx['params'].get('model') == 'res.company':
        #     return super(ResPartnerInherited, self).create(vals)

        if not vals.get('company_id', False) and not vals.get('is_company', False):
            vals.update({'company_id': self.env.user.company_id.id})

        obj = super(ResPartnerInherited, self).create(vals)
        if obj.message_partner_ids:
            obj.message_unsubscribe(obj.message_partner_ids.ids)    
        return obj

    def write(self, vals):
        """ Set the Accounting Full Group as False when Billing User is set. """
        # print("\n Partner Write() >>>>>>")
        # print("vals ===",vals)
        self.ensure_one()
        # print(self.message_partner_ids)
        res = super(ResPartnerInherited, self).write(vals)
        if 'user_id' in vals and self.message_partner_ids:
            self.message_unsubscribe([self.user_id.partner_id.id])
        return res

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        ctx = self._context
        print("\nPARTNER Search()   ctx ====",ctx)
        print("     args =",args, "offset =",offset, "limit =",limit, "order =",order, "count =",count)        
        if self.user_has_groups('base.group_system,base.group_erp_manager') and 'allowed_company_ids' not in ctx: # Return super() while the User is Settings/Accessrights group.
            return super(ResPartnerInherited, self).search(args, offset=offset, limit=limit, order=order, count=count)
        if ctx.get('create_user'): # Return super() while the User is created.
            print("createeeee_user")
            return super(ResPartnerInherited, self).search(args, offset=offset, limit=limit, order=order, count=count)
        if ctx.get('allowed_company_ids') and limit and limit > 1:
            if not args:
                args = [('company_id','in',ctx['allowed_company_ids'])]
            else:
                args[0] = ('company_id','in',ctx['allowed_company_ids'])
        res = super(ResPartnerInherited, self).search(args, offset=offset, limit=limit, order=order, count=count)
        print("     ",args)
        return res

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        ctx = self._context
        print("\nPARTNER read_group()   ctx ====",ctx)
        print("     domain =",domain, "fields =",fields, "groupby =",groupby, "offset =",offset, "limit =",limit, "orderby =",orderby, "lazy =",lazy)        
        if self.user_has_groups('base.group_system,base.group_erp_manager') and 'allowed_company_ids' not in ctx: # Return super() while the User is Settings/Accessrights group.
            return super(ResPartnerInherited, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
        if ctx.get('allowed_company_ids') and limit and limit > 1:
            if not domain:
                domain = [('company_id','in',ctx['allowed_company_ids'])]
            else:
                domain[0] = ('company_id','in',ctx['allowed_company_ids'])        
        return super(ResPartnerInherited, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
    

class PartnerCategory(models.Model):
    _inherit = 'res.partner.category'

    company_id = fields.Many2one("res.company",string="Company",default=lambda self: self.env.company)