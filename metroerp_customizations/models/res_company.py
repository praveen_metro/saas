# -*- coding: utf-8 -*-
import os
from odoo import api, models, tools, fields
from odoo.modules.module import get_resource_path

from random import randrange
from PIL import Image
import base64
import io


class ResCompany(models.Model):
    _inherit = 'res.company'

    sales_tc = fields.Text(translate=True)
    sales_invoice_tc = fields.Text(translate=True)
    purchase_tc = fields.Text(translate=True)
    purchase_invoice_tc = fields.Text(translate=True)
    chart_of_accounts_installed = fields.Boolean(default=False, string="Chart of Accounts Installed")


    def _get_default_favicon(self, original=False):
        img_path = get_resource_path('metroerp_customizations', 'static/src/img/favicon.png')
        with tools.file_open(img_path, 'rb') as f:
            if original:
                return base64.b64encode(f.read())
            color = (randrange(32, 224, 24), randrange(32, 224, 24), randrange(32, 224, 24))
            original = Image.open(f)
            new_image = Image.new('RGBA', original.size)
            height = original.size[1]
            width = original.size[0]
            bar_size = 1
            for y in range(height):
                for x in range(width):
                    pixel = original.getpixel((x, y))
                    if height - bar_size <= y + 1 <= height:
                        new_image.putpixel((x, y), (color[0], color[1], color[2], 255))
                    else:
                        new_image.putpixel((x, y), (pixel[0], pixel[1], pixel[2], pixel[3]))
            stream = io.BytesIO()
            new_image.save(stream, format="PNG")
            return base64.b64encode(stream.getvalue())
        
    favicon = fields.Binary(string="Company Favicon", help="This field holds the image used to display a favicon for a given company.", default=_get_default_favicon)
        
    
    
    @api.model
    def default_get(self, fields_list):
        defaults = super(ResCompany, self).default_get(fields_list)
        chart_template = self.env['account.chart.template'].search([], limit=1)
        if chart_template:
            defaults['chart_template_id'] = chart_template.id

        return defaults
    
    @api.model
    def create(self, vals):
        if not vals.get('favicon'):
            vals['favicon'] = self._get_default_favicon()  
        return super(ResCompany, self).create(vals)
    
    def install_chart_of_accounts(self):
        if self.chart_template_id:
            self.chart_template_id._load(15.0, 15.0, self)
            self.chart_of_accounts_installed = True


    @api.model
    def reset_company_logo(self):
        order_objs = self.search([])
        for order_obj in order_objs:
            order_obj.logo = open(
                os.path.join(tools.config['root_path'], 'addons', 'base', 'res', 'res_company_logo.png'),
                'rb').read().encode('base64')

    def get_logo_data_url(self):
        url = image_data_uri(self.logo)
        return url

    @api.model
    def create(self, vals):
        ctx = self._context
        obj = super(ResCompany, self).create(vals)        
        obj.sudo().partner_id.write({'company_id': obj.id})
        return obj