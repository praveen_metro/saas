# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class Stock(models.Model):
    _inherit = 'stock.picking'

    def _get_total_item(self):
        total = 0.0
        for line in self.move_lines:
            total += line.product_uom_qty
        return total