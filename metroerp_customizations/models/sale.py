# -*- coding: utf-8 -*-

from odoo import api, fields, models
from datetime import datetime


class SaleOrder(models.Model):
    _inherit = "sale.order"

    create_date_tmp = fields.Datetime('Creation Date')

    # def create(self, vals):
    #     res = super(SaleOrder, self).create(vals)
    #     if 'create_date_tmp' in vals:
    #         self.env.cr.execute("UPDATE sale_order set create_date = %s where id = %s", (vals.get('create_date_tmp'), res.id))
    #     else:
    #         res.create_date_tmp = res.create_date
    #     return res

    @api.model
    def _sales_default_note(self):
        return self.env['ir.config_parameter'].sudo().get_param('use_sales_tc') and self.env.company.sales_tc or ''

    note = fields.Text('Terms and conditions', default=_sales_default_note)

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        """ Inherited Method """
        res = super(SaleOrder, self).onchange_partner_id()
        if self.env['ir.config_parameter'].sudo().get_param('use_sales_tc') and self.env.company.sales_tc:
            self.update({'note': self.with_context(lang=self.partner_id.lang).env.company.sales_tc})
        return res
    
    def action_confirm_sale_order(self):
        for rec in self:
            if rec.state in ['draft','sent']:
                rec.state = 'sale'

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.onchange('product_id')
    def product_id_change(self):
        """ Inherited Method """
        result = super(SaleOrderLine, self).product_id_change()
        if not self.product_id.description_sale:
            self.name = " "
        elif self.product_id.description_sale:
            description_sale = self.product_id.description_sale.strip()
            if not description_sale:
                description_sale = " "
            self.name = description_sale
        return result


class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

