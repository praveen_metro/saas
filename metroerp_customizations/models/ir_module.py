# -*- coding: utf-8 -*-
from odoo import models, fields, api


class Module(models.Model):
    _inherit = "ir.module.module"

    to_buy = fields.Boolean('ERP Enterprise Module', default=False)

    @api.model
    def search(self, domain, offset=0, limit=None, order=None, count=False):
        domain += [("to_buy", "=", False)]
        return super().search(domain, offset, limit, order, count)
    
