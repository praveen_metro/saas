from odoo import models,fields,api,_

class AccountJournal(models.Model):
    _inherit = 'account.journal'

    @api.model
    def _default_invoice_reference_model(self):
        """Override the default invoice reference model."""
        country_code = self.env.company.country_id.code
        country_code = country_code and country_code.lower()
        if country_code:
            for model in self._fields['invoice_reference_model'].get_values(self.env):
                if model.startswith(country_code):
                    return model
        return 'metro'  # Replace 'odoo' with 'metro'
    
    @api.model
    def _get_new_selection_type(self):
        selection = [
            ('metro', 'Metro'),
            ('euro', 'European')]
        return selection

    invoice_reference_model = fields.Selection(selection='_get_new_selection_type',
        string='Communication Standard',
        required=True,
        default=_default_invoice_reference_model,
        help="You can choose different models for each type of reference. The default one is the Metro reference."
    )

    

    


    

    